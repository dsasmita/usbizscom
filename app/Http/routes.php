<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['namespace' => 'Admin','prefix' => 'admin'], function () {
	Route::get('scraper-state', ['as' => 'admin.state', 'uses' => 'scraperController@state']);

	Route::get('scraper-city', ['as' => 'admin.city', 'uses' => 'scraperController@city']);
	
	Route::get('scraper-city-formated', ['as' => 'admin.city', 'uses' => 'scraperController@cityFormated']);
	
	Route::get('scraper-detail', ['as' => 'admin.detail', 'uses' => 'scraperController@detail']);
});

Route::auth();

Route::group(['namespace' => 'Front'], function () {
	Route::get('/', ['as' => 'front.home', 'uses' => 'homeController@index']);
	Route::get('states', ['as' => 'front.states', 'uses' => 'homeController@states']);
	Route::get('cities', ['as' => 'front.cities', 'uses' => 'homeController@cities']);
	
	Route::get('get-cities', ['as' => 'front.cities.get', 'uses' => 'homeController@getCities']);
	
	Route::get('new-business', ['as' => 'front.create', 'uses' => 'homeController@newBusiness']);
	Route::post('new-business', ['as' => 'front.create.post', 'uses' => 'homeController@newBusinessPost']);

	Route::get('sitemap-state.xml', ['as' => 'sitemap.state', 'uses' => 'sitemapController@state']);
	Route::get('sitemap-city.xml', ['as' => 'sitemap.city', 'uses' => 'sitemapController@city']);
	Route::get('sitemap-business.xml', ['as' => 'sitemap.business', 'uses' => 'sitemapController@business']);
	
	Route::get('{id}', ['as' => 'front.state.detail', 'uses' => 'homeController@stateDetail']);
	Route::get('{id}/{id2}', ['as' => 'front.city.detail', 'uses' => 'homeController@cityDetail']);
	Route::get('{id}/{id2}/{id3}', ['as' => 'front.business', 'uses' => 'homeController@business']);
	
});