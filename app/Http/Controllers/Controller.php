<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    protected $title = 'United State - Business  Directory';
    protected $limit = 10;
    
    protected $seoPack = [
    						'title'       => 'United State - Business Directory: USA Business Directory and Discover the Opportunities for Business and Business Cooperation in USA',

    						'description' => 'United State - Business  Directory: information center business address United State. Expand business network with List Business and introduce business to all over United State, find business opportunity and business offer from business all over United State, get latest business trend information',

    						'keywords'    => 'Business Directory, Local Business Directory, Online Business Directory, Business Directories, Small Business Directory, Columbia Business School Directory, Google Business Directory, Business Directory Plugin, Wordpress Business Directory, Free Business Directory, Black Owned Business Directory, Cortera Business Directory, Business Directory Listings, Armenian Business Directory, Business Directory Software, Yahoo Business Directory, Dubai Business Directory, The Directory Service Is Busy, Local Business Directories, Veteran Owned Business Directory, Us Business Directory, Best Business Directory, Florida Business Directory, Minority Owned Business Directory, California Business Directory, Open Business Directory Ltd, Wordpress Business Directory Plugin, Online Business Directories, Yelp Business Directory, List Of Business Directories In Usa, Business Directory Of Uae, Houston Business Directory, Business Directory Sweden, Cleveland Ohio Business Directory, A To Z Business Directory, Denver Small Business Directory, Small Business Owners Directory, South Bend Business Directory, Business Directory Listing Software, Bulgaria Business Directory, Maryland Small Business Directory, Midlothian Business Directory, Long Island Business Directory, How To Build A Business Directory Website, Free Business Directory Listing, Business Email Directory Free, Scottsdale Business Directory, Chicago Business Directory, Kingston Ontario Business Directory, Dc Business Directory, Business Phone Number Directory, Free Business Directories, Create Business Directory Website, Tstt Business Directory, Business Phone Directory, Christian Business Directory Las Vegas, Business Telephone Directory Usa, Home Party Business Directory, Business Directory Italy'
    					];
}
