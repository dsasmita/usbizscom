<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Sunra\PhpSimple\HtmlDomParser;

use DB;

use App\Models\state;
use App\Models\city;
use App\Models\scrapLog;
use App\Models\business;

class scraperController extends Controller
{
    public function state(Request $request){
    	$html 	= HtmlDomParser::file_get_html('http://www.usbizs.com/state.html');

    	// ini_set('max_execution_time', 0);
    	$states = [];
    	foreach($html->find('.statel li') as $element){
    		$check = state::where('name', $element->find('a',0)->plaintext)->first();
    		if(!$check){
	    		$state = [];
	    		$img  = explode('/', $element->find('img',0)->src);
				$state['img'] = 'flag/'.$img[count($img)-1];
	    		
	    		$state['img_path']  = $element->find('img',0)->src;
	    		$state['href'] = $element->find('a',0)->href;
	    		$state['name'] = $element->find('a',0)->plaintext;

				$shortState  = explode('/', $state['href']);
				$state['citation'] = $shortState[count($shortState)-2];

	    		$base_url = 'uploads/flag/';
				$exist = file_exists(public_path($base_url));
				if(!$exist){
					mkdir($base_url, 0777, true);
				}

				$ch    = curl_init($state['img_path']);
				$name  = explode('/', $state['img_path']);
				$count = count($name)-1;

				$fileExist = file_exists(public_path($base_url.$name[$count]));
				if(!$fileExist){
					$fp = fopen($base_url.$name[$count], 'wb');
					curl_setopt($ch, CURLOPT_FILE, $fp);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_exec($ch);
					curl_close($ch);
					fclose($fp);
				}

	    		$html2 	= HtmlDomParser::file_get_html($state['href']);
	    		foreach ($html2->find('.pageNav a') as $el) {
	    			if($el->plaintext == 'Last'){
	    				$href_last = $el->href;
	    			}
	    		}
	    		$tmp = str_replace("/".$state['citation'].'/index-',"",$href_last);
	    		$tmp = str_replace(".html","",$tmp);

	    		$state['href_last'] = $tmp;

	    		$states[] = $state;
		    	$s = state::create($state);
	    	}
       	}
       	// dd($states);
       	return '<hr>done';
    }

    public function city(Request $request){
    	return 'exit';

		// ini_set('max_execution_time', 0);
    	$cities = [];
    	for ($i=240; $i <= 460; $i++) {
    		$url = 'http://www.usbizs.com/city.html';
    		if($i > 1){
    			$url = 'http://www.usbizs.com/city-'.$i.'.html';
    		}
    		$html 	= HtmlDomParser::file_get_html($url);
	    	foreach($html->find('.statel li') as $el){
	    		if(city::where('name',$el->find('a',0)->plaintext)->count() == 0){
		    		$city = [];
		    		$city['name'] 	   = $el->find('a',0)->plaintext;
		    		
		    		$listed = str_replace('(', '', $el->find('span',0)->plaintext);
		    		$listed = str_replace(')', '', $listed);
		    		$city['listed']    = (int) $listed;
		    		
		    		$city['href'] 	   = 'http://www.usbizs.com/'.$el->find('a',0)->href;

		    		$tmp = explode('/',$el->find('a',0)->href);
		    		$city['state_id'] = $tmp[0];

		    		$city['href_last'] = 0;
		    		$city['citation']  = str_slug($city['name']);

		    		$cities[] = $city;
		    		$ct = city::create($city);
	    		}
	    	}
    	}
    	dd($cities);
    	return '<hr>done';
    }

    public function cityFormated(){
    	// ini_set('max_execution_time', 0);
    	$cities = city::where('href_last',0)->where('name','like','%...%')->limit(100)->get();
    	foreach ($cities as $li) {
    		$href_last = 1;
    		$html2 	= HtmlDomParser::file_get_html($li->href);
    		foreach ($html2->find('.pageNav a') as $el) {
    			if($el->plaintext == 'Last'){
    				$href_last = $el->href;
    			}
    		}

    		if($href_last != 1){
    			$href = str_replace("http://www.usbizs.com","",$li->href);
    			$href = str_replace(".html","",$href);

    			$tmp = str_replace($href.'-',"",$href_last);
    			$tmp = str_replace(".html","",$tmp);

    			$href_last = $tmp;
    		}

    		$li->name =  $html2->find('.bizrt span',0)->plaintext;
    		$li->citation = str_slug($html2->find('.bizrt span',0)->plaintext);
    		$li->href_last = $href_last;
    		$li->save();
    	}

    	return 'done';
    }

    public function detail(Request $request){
    	// ini_set('max_execution_time', 0);

    	if($request->input('url') == ''){
	    	$cities = city::where('href_last','!=',0)->limit(100)->get();

	    	echo "<ul>";
	    	foreach ($cities as $li) {
	    		$biz = business::where('city', $li->name)->count();
	    		echo "<li><a target='_blank' href='".route('admin.detail')."?url=". $li->href."&limit=".$li->href_last."'>".
	    				$li->name." - ".$li->listed."</a><br>Count:".$biz."</li>";
	    	}
	    	echo "</ul>";
	    	return 'done';
    	}
    	$url = str_replace('.html', '', $request->input('url'));

    	for ($i=env('SCRAP_DETAIL_LAST'); $i <= $request->input('limit'); $i++) {
    		DB::transaction(function () use($url, $i){
	    		$pages = $url.'-'.$i.'.html';
		    	$check_link = scrapLog::where('pages',$pages)->first();

		    	if(!$check_link){
		    		$scrapLog = scrapLog::create(['pages' => $pages]);

		    		$html 	= HtmlDomParser::file_get_html($pages);

		    		$y = 0;
			    	foreach($html->find('.bizrmainc table.list') as $el){
			    		$check = business::where('href', trim(@$el->find('a',0)->href))->first();
			    		if(!$check){
				    		$biz = [];
				    		$biz['name'] = trim(@$el->find('a',0)->plaintext);
				    		$biz['href'] = trim(@$el->find('a',0)->href);
				    		$biz['slug'] = str_slug(trim($biz['name'])).'_'.$this->generateRandomString(6);
				    		
				    		$biz['street_address'] = trim(@$el->find('[itemprop=streetAddress]',0)->plaintext);
				    		$biz['city']  = trim(@$el->find('[itemprop=addressLocality]',0)->plaintext);
				    		$biz['city_citation']  = str_slug(trim(@$el->find('[itemprop=addressLocality]',0)->plaintext));
				    		$biz['state'] = trim(@$el->find('[itemprop=addressRegion]',0)->plaintext);

				    		$state = state::where('name', $biz['state'])->first();
				    		if($state)
					    		$biz['state_citation'] = $state->citation;

				    		$biz['country'] = trim(@$el->find('[itemprop=addressCountry]',0)->plaintext);
				    		$biz['postal_code'] =trim(@$el->find('[itemprop=postalCode]',0)->plaintext);
				    		$biz['phone'] = trim(@$el->find('[itemprop=telephone]',0)->plaintext);
				    		$biz['fax'] = trim(@$el->find('[itemprop=faxNumber]',0)->plaintext);

				    		$bz = business::create($biz);
			    		}
			    		$y++;
			       	}
			       	if($y == 0){
			       		DB::rollBack();
			       		echo 'failed scrapt '.$pages.'<br>';
			       		// echo HtmlDomParser::file_get_html($pages); 
			       		echo '<iframe src="'.$pages.'"></iframe>';

			       		// echo '
			       		// 	<div style="text-align:center; margin-top:100px;line-height:200%;">
			       		// 		<span style="font-weight:bold;">Please Sumbit Verification Code to Continued View.</span>
			       		// 		<br />
			       		// 		<img id="code" src="http://www.usbizs.com/includes/code.php" alt="Click to Change a Picture" style="cursor: pointer; vertical-align:middle;" onClick="create_code()"/>
			       		// 		<form target="_blank" action="'.$pages.'" method="post">
			       		// 			<input type="text" size="6" name="code" />
			       		// 			<button type="submit" style="padding:3px;line-height:16px">Submit</button>
			       		// 		</form>
			       		// 	</div>
			       		// 	<script>function create_code(){document.getElementById(\'code\').src =\'/includes/code.php?act=ver&\'+Math.random()*10000;}</script>';
			       		
			       		exit;
			       	}
		    	}
	    	});
    	}
    	return 'done';
    }

    public function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
}
