<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\state;
use App\Models\city;
use App\Models\business;

class sitemapController extends Controller
{
    public function state(Request $request){
    	$sitemap = \App::make("sitemap");

    	$states = state::orderBy('updated_at', 'desc')->get();
        foreach ($states as $li)
        {
            $sitemap->add(route('front.state.detail',[$li->citation]), $li->updated_at, '0.9', 'daily');
        }

	    return $sitemap->render('xml');
    }

    public function city(Request $request){
    	$sitemap = \App::make("sitemap");

    	$city = city::orderBy('listed', 'desc')->paginate(10000);
        foreach ($city as $li)
        {
            $sitemap->add(route('front.city.detail',[$li->state_id, $li->citation]), $li->updated_at, '0.9', 'daily');
        }

	    return $sitemap->render('xml');
    }

    public function business(Request $request){
        $sitemap = \App::make("sitemap");

        $city = business::orderBy('created_at', 'desc')->paginate(5000);
        foreach ($city as $li)
        {
            $sitemap->add(route('front.business',[$li->state_citation, $li->city_citation, $li->slug]), $li->updated_at, '0.9', 'daily');
        }

        return $sitemap->render('xml');
    }
}
