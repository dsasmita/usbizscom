<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;

use App\Models\state;
use App\Models\city;
use App\Models\business;

class homeController extends Controller
{
    public function index(Request $request){
    	$arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['url'] = $request->url();
        $arr_data['seo']['image'] = asset('assets/images/banner.jpg');

    	$arr_data['title'] = 'United State - Business  Directory';

    	$arr_data['states'] = state::orderBy('name','asc')->get();
    	return view('front.home', $arr_data);
    }

    public function states(Request $request){
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['url'] = $request->url();
        $arr_data['seo']['image'] = asset('assets/images/banner.jpg');

        $arr_data['title'] = 'States - United State - Business  Directory';

        $arr_data['states'] = state::orderBy('name','asc')->get();
        return view('front.home', $arr_data);
    }

    public function cities(Request $request){
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['url'] = $request->url();
        $arr_data['seo']['image'] = asset('assets/images/banner.jpg');

        // SEO
        $arr_data['seo']['title']       = 'City @ United State | '.$this->title;
        $arr_data['seo']['keywords']    = 'City @ United State, '.$arr_data['seo']['keywords'];
        
        $arr_data['title'] = 'City @ United State - Business  Directory';
        $arr_data['cities'] = city::orderBy('listed','desc')->paginate(120);

        return view('front.city-list', $arr_data);
    }

    public function stateDetail($state ,Request $request){
    	$state = state::where('citation', $state)->first();
    	if(!$state){
    		abort(404, 'State not listed');
    	}

    	$arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['url'] = $request->url();
        $arr_data['seo']['image'] = asset('assets/images/banner.jpg');

        $arr_data['state'] = $state;
    	$arr_data['title'] = 'State '.$state->name.' @ United State - Business  Directory';
    	$arr_data['cities'] = city::where('state_id', $state->citation)->orderBy('listed','desc')->paginate(120);

        // SEO
        $arr_data['seo']['title']       = $state->name.' @ United State | '.$this->title;
        $arr_data['seo']['keywords']    = 'State: ' . $state->name.' Business Directory @ United State, '.$arr_data['seo']['keywords'];
        $arr_data['seo']['description']    = 'State: ' . $state->name.' Business Directory @ United State, '.$arr_data['seo']['description'];

    	return view('front.state-detail', $arr_data);
    }

    public function cityDetail($state, $city, Request $request){
    	$city = city::where('state_id', $state)->where('citation',$city)->first();
    	if(!$city){
    		abort(404, 'City not listed');
    	}

    	$arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['url'] = $request->url();
        $arr_data['seo']['image'] = asset('assets/images/banner.jpg');

        $arr_data['city'] = $city;
    	$arr_data['title'] = 'City '.$city->name.' @ United State - Business  Directory';
    	$arr_data['business'] = business::where('state_citation', $city->state_id)
    							->where('city_citation', $city->citation)
    							->orderBy('updated_at','desc')->paginate(35);

        // SEO
        $arr_data['seo']['title']       = 'City '. $city->name.' @ United State | '.$this->title;
        $arr_data['seo']['keywords']    = ' City ' . $city->name.' Business Directory @ United State, '.$arr_data['seo']['keywords'];
        $arr_data['seo']['description']    = 'City ' . $city->name.' Business Directory @ United State, '.$arr_data['seo']['description'];

    	return view('front.city-detail', $arr_data);
    }

    public function business($state, $city, $slug, Request $request){
        $business = business::where('state_citation', $state)
                                ->where('city_citation', $city)
                                ->where('slug', $slug)->first();
        if(!$business){
            abort(404, 'Business not listed');
        }

        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['url'] = $request->url();
        $arr_data['seo']['image'] = asset('assets/images/banner.jpg');
        
        $arr_data['title'] = 'Profile and Contact '. $business->name.' @ United State - Business  Directory';
        $arr_data['business'] = $business;

        // SEO
        $arr_data['seo']['title']       = $business->name.' @ United State | '.$this->title;
        $arr_data['seo']['keywords']    = $business->name.' @ United State, '.$arr_data['seo']['keywords'];
        $arr_data['seo']['description']    = $business->name.' @ United State, '.$arr_data['seo']['description'];

        return view('front.business-detail', $arr_data);
    }

    public function newBusiness(Request $request){
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['url'] = $request->url();
        $arr_data['seo']['image'] = asset('assets/images/banner.jpg');

        // SEO
        $arr_data['seo']['title']       = 'New Business @ United State | '.$this->title;
        $arr_data['seo']['keywords']    = 'New Business @ United State, '.$arr_data['seo']['keywords'];
        $arr_data['seo']['description'] = 'New Business @ United State, '.$arr_data['seo']['description'];

        $arr_data['select']['states'] = state::orderBy('name','asc')->get();
        $arr_data['select']['cities'] = [];
        if(old('state') != ''){
            $city = city::where('state_id', old('state'))->orderBy('listed','desc')->limit(200)->get();
            $tmp = [];
            foreach ($city as $key => $li) {
                $tmp[] = ['name' => $li->name, 'citation' => $li->citation];
            }
            $arr_data['select']['cities'] = $tmp;
        }
        
        $arr_data['title'] = 'New Business @ United State - Business  Directory';

        return view('front.new-business', $arr_data);
    }

    public function newBusinessPost(Request $request){
        $input = $request->all();

        $roles = [
                'name' => 'required',
                'state' => 'required',
                'city' => 'required',
                'street_address' => 'required',
                'postal_code' => 'required',
                'phone' => 'required',
                'fax' => 'required',
                'email' => 'required',
                'description' => 'required',
                'product' => 'required',
                'category' => 'required',
                ];

        $validator = Validator::make(
                        $input, $roles
        );

        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)->withInput();
        }

        $biz = [];
        $biz['name'] = trim($request->input('name'));
        $biz['slug'] = str_slug(trim($request->input('name'))).'_'.$this->generateRandomString(6);
        
        $biz['street_address'] = trim($request->input('street_address'));
        
        $city = city::where('citation',$request->input('city'))->first();
        
        $biz['city']  = trim($city->name);
        $biz['city_citation']  = str_slug($request->input('city'));

        $state = state::where('citation', $request->input('state'))->first();
        $biz['state_citation'] = $state->citation;
        $biz['state'] = trim($state->name);

        $biz['country'] = 'United States';
        $biz['postal_code'] =trim($request->input('postal_code'));
        $biz['phone'] = trim($request->input('phone'));
        $biz['fax'] = trim($request->input('fax'));
        $biz['email'] = trim($request->input('email'));

        $bz = business::create($biz);

        \Session::flash('notif-success', 'New Business created.');
        return redirect(route('front.create'));
    }

    public function getCities(Request $request){
        $state = $request->input('state');

        $city = city::where('state_id', $state)->orderBy('listed','desc')->limit(200)->get();

        $tmp = [];

        foreach ($city as $key => $li) {
            $tmp[] = ['name' => $li->name, 'citation' => $li->citation];
        }

        return response()->json([
                            'method'    => 'GET',
                            'status'    => 'OK',
                            'state'     => $state,
                            'data'      => $tmp,
                            'http_code' => 201
                          ],201);
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
