<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class scrapLog extends Model
{
	
	protected $table = 'scrap_log';

	protected $guarded = [];
}
