@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('layouts.sidebar-nav')
        <div class="col-md-9">
            <ol class="breadcrumb">
                <li><a href="{{ route('front.home') }}">Home</a></li>
                <li><a href="{{ route('front.state.detail',$city->state_id) }}">{{$city->state_id}}</a></li>
                <li class="active">{{$city->name}}</li>
            </ol>
            <div class="panel panel-default">
                <div class="panel-heading">{{$city->name}} - {{$city->state_id}} @ U.S.A Business Directory</div>
                <div class="panel-body">
                    {{$city->name}} - {{$city->state_id}} @ U.S.A Business Directory and Discover the Opportunities for Business and Business Cooperation,
                    with thousand companies, complete with contact and maps around.
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h1>{{$city->name}} - {{$city->state_id}}: Business Directory</h1></div>
                <div class="panel-body">
                    <div class="row">
                        @foreach(@$business as $li)
                        <div class="col-md-12 business-list">
                            <h2><a href="{{ route('front.business',[$li->state_citation, $li->city_citation, $li->slug]) }}">{{$li->name}}</a></h2>
                            <span>
                                <strong>Address:</strong> {{$li->street_address}}
                            </span><br>
                            <strong>City:</strong> <a href="{{ route('front.state.detail', $li->state_citation) }}">{{$li->city}}</a> - 
                            <strong>State:</strong> <a href="{{ route('front.city.detail', [$li->state_citation, $li->city_citation]) }}">{{$li->state}}</a> - 
                            <strong>Country:</strong> United States 
                            <strong>Zipcode:</strong> {{$li->postal_code}}
                            <div class="row">
                                @if($li->phone != '')
                                <div class="col-md-4"><strong>Tell: </strong>{{$li->phone}}</div>
                                @endif
                                @if($li->fax != '')
                                    <div class="col-md-4"><strong>Fax: </strong>{{$li->fax}}</div>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div><center>{{ $business->links() }}</center></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
