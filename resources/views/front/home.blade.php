@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('layouts.sidebar-nav')
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">United States of America Business Directory</div>
                <div class="panel-body">
                    United States (U.S.A) Business Directory and Discover the Opportunities for Business and Business Cooperation,
                    with thousand companies, complete with contact and maps around.
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h1>United States of America States</h1></div>
                <div class="panel-body">
                    <div class="row">
                        @php
                        $i = 1;
                        @endphp

                        @foreach(@$states as $li)
                        <div class="col-md-3 col-sm-4 list-state">
                            <img src="{{ url('assets/'.$li->img) }}" width="20px" alt="flag {{$li->name}}">
                            <a href="{{route('front.state.detail',$li->citation)}}">
                                {{$li->name}}
                            </a>
                        </div>

                        @if(env('ADS_STATUS') == 'RUN')
                            @if($i == 12 || $i == 24 || $i == 36)
                            <div class="col-md-12">
                                <center>
                                    -ads-
                                </center>
                            </div>
                            @endif
                        @endif

                        @php
                        $i++;
                        @endphp

                        @endforeach
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
