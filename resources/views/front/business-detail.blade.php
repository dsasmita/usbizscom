@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('layouts.sidebar-nav')
        <div class="col-md-9">
            <ol class="breadcrumb">
                <li><a href="{{ route('front.home') }}">Home</a></li>
                <li><a href="{{ route('front.state.detail',$business->state_citation) }}">{{$business->state_citation}}</a></li>
                <li><a href="{{ route('front.city.detail',[$business->state_citation, $business->city_citation]) }}">{{$business->city}}</a></li>
                <li class="active">{{$business->name}}</li>
            </ol>
            <div class="panel panel-default">
                <div class="panel-heading"><h1>{{$business->name}} @ {{$business->city}} - {{$business->state}} U.S.A</h1></div>
                <div class="panel-body">
                    {{$business->name}}: business located @ {{$business->street_address}} {{$business->city}} - {{$business->state}} U.S.A
                    you can find detail about contact, detail information and maps below
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Profile: {{$business->name}}</div>
                        <div class="panel-body">
                            <ul class="profile">
                                <li><h2>{{$business->name}}</h2></li>
                                <li><strong>Country: </strong> United States</li>
                                <li><strong>City: </strong> <a href="{{ route('front.city.detail',[$business->state_citation, $business->city_citation]) }}">{{$business->city}}</a></li>
                                <li><strong>State:</strong> <a href="{{ route('front.state.detail',$business->state_citation) }}">{{$business->state}}</a> - {{$business->state_citation}}</li>
                                <li><strong>Address:</strong> {{$business->street_address}}</li>
                                <li><strong>Zipcode:</strong> {{$business->postal_code}}</li>
                                <li><strong>Category:</strong> {{$business->category == '' ? '-not set-' : $business->category}}</li>
                                <li><strong>Products:</strong> {{$business->description == '' ? '-not set-' : $business->description}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Contact: {{$business->name}}</div>
                        <div class="panel-body">
                            <ul class="profile">
                                <li><strong>Phone:</strong> {{$business->phone == '' ? '-not set-' : $business->phone}}</li>
                                <li><strong>Fax:</strong> {{$business->fax == '' ? '-not set-' : $business->fax}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
