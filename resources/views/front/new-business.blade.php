@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('layouts.sidebar-nav')
        <div class="col-md-9">
            <ol class="breadcrumb">
                <li><a href="{{ route('front.home') }}">Home</a></li>
                <li class="active">New Business</li>
            </ol>
            <div class="panel panel-default">
                <div class="panel-heading">New Business @ U.S.A Business Directory</div>
                <div class="panel-body">
                    New Business @ U.S.A Business Directory and Discover the Opportunities for Business and Business Cooperation,
                    with thousand companies, complete with contact and maps around.
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"><h1>New Business @ U.S.A</h1></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-9">
                          
                          @if(Session::get('notif-error'))
                            <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <strong>Notification!</strong> {{Session::get('notif-error')}}.
                            </div>
                          @endif
                          @if(Session::get('notif-warning'))
                            <div class="alert alert-warning alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <strong>Notification!</strong> {{Session::get('notif-warning')}}.
                            </div>
                          @endif
                          @if(Session::get('notif-success'))
                             <div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <strong>Notification!</strong> {{Session::get('notif-success')}}.
                            </div>
                          @endif
                          @foreach ($errors->all() as $message)
                              <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Notification!</strong> {{$message}}.
                              </div>
                          @endforeach
                          <form class="form-horizontal" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                              <label for="name" class="col-sm-3 control-label">Company Name</label>
                              <div class="col-sm-9">
                                <input type="text" value="{{ old('name') }}" name="name" class="form-control" id="name" placeholder="company Name">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="state" class="col-sm-3 control-label">State</label>
                              <div class="col-sm-9">
                                  <select class="form-control" name="state" id="state">
                                      <option value="">-select state-</option>
                                      @foreach($select['states'] as $li)
                                          <option {{ old('state') == $li->citation ? 'selected' : '' }} value="{{$li->citation}}">{{$li->name}}</option>
                                      @endforeach
                                  </select>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="city" class="col-sm-3 control-label">City</label>
                              <div class="col-sm-9">
                                  <select class="form-control" name="city" id="city">
                                      <option value="">-select city-</option>
                                      @foreach($select['cities'] as $li)
                                          <option {{ old('city') == $li['citation'] ? 'selected' : '' }} value="{{$li['citation']}}">{{$li['name']}}</option>
                                      @endforeach
                                  </select>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="street_address" class="col-sm-3 control-label">Street Address</label>
                              <div class="col-sm-9">
                                <input type="text" value="{{ old('street_address') }}" name="street_address" class="form-control" id="street_address" placeholder="address">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="postal_code" class="col-sm-3 control-label">Postal Code</label>
                              <div class="col-sm-9">
                                <input type="text" value="{{ old('postal_code') }}" class="form-control" name="postal_code" id="postal_code" placeholder="postal code">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="phone" class="col-sm-3 control-label">Phone</label>
                              <div class="col-sm-9">
                                <input type="text" value="{{ old('phone') }}" name="phone" class="form-control" id="phone" placeholder="phone">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="fax" class="col-sm-3 control-label">Fax</label>
                              <div class="col-sm-9">
                                <input type="text" value="{{ old('fax') }}" class="form-control" name="fax" id="fax" placeholder="fax">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="email" class="col-sm-3 control-label">Email</label>
                              <div class="col-sm-9">
                                <input type="text" value="{{ old('email') }}" name="email" class="form-control" id="email" placeholder="email">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="description" class="col-sm-3 control-label">Description</label>
                              <div class="col-sm-9">
                                  <textarea class="form-control" name="description" id="description" placeholder="description">{{ old('description') }}</textarea>
                               </div>
                            </div>
                            <div class="form-group">
                              <label for="product" class="col-sm-3 control-label">Product</label>
                              <div class="col-sm-9">
                                  <textarea class="form-control" name="product" id="product" placeholder="product">{{ old('product') }}</textarea>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="category" class="col-sm-3 control-label">Category</label>
                              <div class="col-sm-9"> 
                                <input type="text" value="{{ old('category') }}" name="category" class="form-control" id="category" placeholder="category Name">
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-primary btn-block">Submit</button>
                              </div>
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $('#state').on('change', function() {
          var state = this.value;

          $.ajax({
                dataType: "json",
                type: "GET",
                url: "{{ route('front.cities.get') }}?state="+state,
            }).done(function(result) {
                if(result.status == 'OK'){
                    var $el = $("#city");
                    $el.empty();
                    $el.append($("<option></option>")
                         .attr("value", '').text('-select city-'));
                    $.each(result.data, function(key,value) {
                      $el.append($("<option></option>")
                         .attr("value", value.citation).text(value.name));
                    });
                }else{
                    alert('Something bad happend!');
                }
            });
        })
    });
</script>
@endsection