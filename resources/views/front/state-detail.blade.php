@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('layouts.sidebar-nav')
        <div class="col-md-9">
            <ol class="breadcrumb">
                <li><a href="{{ route('front.home') }}">Home</a></li>
                <li class="active">{{$state->name}}</li>
            </ol>
            <div class="panel panel-default">
                <div class="panel-heading">{{$state->name}} @ U.S.A Business Directory</div>
                <div class="panel-body">
                    <center><img class="img-responsive img-thumbnail" width="150px" src="{{ url('assets/'.$state->img) }}" alt="{{$state->name}}"></center>
                    <center>
                    {{$state->name}} @ U.S.A Business Directory and Discover the Opportunities for Business and Business Cooperation,
                    with thousand companies, complete with contact and maps around.
                    </center>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"><h1>{{$state->name}} @ U.S.A Listing</h1></div>
                <div class="panel-body">
                    <div class="row">
                        @php
                        $i = 1;
                        @endphp

                        @foreach(@$cities as $li)
                        <div class="col-md-3 col-sm-4 list-state">
                            <a href="{{route('front.city.detail',[$li->state_id,$li->citation])}}">
                                {{$li->name}}
                            </a>
                        </div>

                        @if(env('ADS_STATUS') == 'RUN')
                            @if($i % 12 == 0 && $i <= 4*12)
                            <div class="col-md-12">
                                <center>
                                    -ads-
                                </center>
                            </div>
                            @endif
                        @endif

                        @php
                        $i++;
                        @endphp

                        @endforeach
                    </div>
                    <div><center>{{ $cities->links() }}</center></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
