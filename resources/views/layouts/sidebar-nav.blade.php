<div class="col-md-3 hidden-xs hidden-sm">
    <div class="panel panel-default">
        <div class="panel-heading">Navigation</div>

        <div class="panel-body">
        	<ul class="sidenav-category">
        	@foreach(\App\Models\state::inRandomOrder()->limit(20)->get() as $li)
        		<li><a href="{{route('front.state.detail',$li->citation)}}">{{$li->name}}</a></li>
        	@endforeach
        	</ul>
        </div>
    </div>
</div>