@if(env('APP_ENV') != 'production')
    <meta name="robots" content="noindex,nofollow">
    <meta name="googlebot" content="noindex,nofollow" />
@else
    <meta name="robots" content="index,follow">
    <meta name="googlebot" content="index,follow" />
@endif
<link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="United State - Business  Directory">
<meta property='og:site_name' content='United State - Business  Directory' />
<meta property="og:title" content="{{ @$seo['title'] }}" />
<meta property="og:description" content="{{@$seo['description']}}" />
<meta property="og:url" content="{{ @$seo['url'] }}" />
<meta property="og:image" content="{{ @$seo['image'] }}" />
<meta name="keywords" content="{{ @$seo['keywords'] }}" />
<meta name="description" content="{{ @$seo['description'] }}" />
<meta name="copyright" content="Copyright United State - Business  Directory" />
<meta name="language" content="EN" />
<meta name="distribution" content="Global" />
<meta name="rating" content="General" />
<meta name="expires" content="never" />
<link rel="canonical" href="{{ @$seo['url'] }}"/>